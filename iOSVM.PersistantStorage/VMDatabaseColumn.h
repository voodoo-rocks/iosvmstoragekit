//
//  VMDatabaseColumn.h
//  iOSVM.PersistantStorage
//
//  Created by Olesya Kondrashkina on 25/06/15.
//  Copyright (c) 2015 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMDatabaseColumn : NSObject

@property (copy) NSString *name;
@property (copy) NSString *type;
@property (copy) NSString *className;

- (id)initWithName:(NSString *)name andType:(NSString *)type andClassName:(NSString *)className;

@end
