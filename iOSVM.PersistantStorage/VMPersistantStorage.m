//
//  VMPersistantStorage.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 03/10/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMPersistantStorage.h"
#import "FMDB.h"

@implementation VMPersistantStorage

static VMPersistantStorage *_persistanceStorage;

- (id)init
{
	self = [super init];
	if (self) {

		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths firstObject];
		NSString *fileName = [documentsDirectory stringByAppendingPathComponent:@"storage.sqlite"];
		
		queue = [FMDatabaseQueue databaseQueueWithPath:fileName];
	}
	return self;
}

+ (VMPersistantStorage *)sharedStorage;
{
	if (!_persistanceStorage) {
		_persistanceStorage = [VMPersistantStorage new];
	}
	
	return _persistanceStorage;
}

- (void)addColumn:(VMDatabaseColumn *)column withCallback:(VMCompletionBlock)block
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [queue inDatabase:^(FMDatabase *database) {
            BOOL result = [self addColumn:column inDatabase:database];
            if (!result) {
                NSLog(@"%@", [database lastError]);
            }
            if (block) {
                if (result) {
                    block (YES, nil);
                }
                else {
                    block (NO, [database lastError]);
                }
            }
        }];
    }];
}

- (void)addColumnsArray:(NSArray *)columns withCallback:(VMCompletionBlock)block
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [queue inTransaction:^(FMDatabase *database, BOOL *rollback) {
            for (VMDatabaseColumn *column in columns) {
                if (![self addColumn:column inDatabase:database]) {
                    NSLog(@"%@", [database lastError]);
                    if (block) {
                        block (NO, [database lastError]);
                    }
                    *rollback = YES;
                    return;
                }
            }
            if (block) {
                block (YES, nil);
            }
        }];
    }];
}

- (BOOL)addColumn:(VMDatabaseColumn *)column inDatabase:(FMDatabase *)database
{
    if ([database columnExists:column.name inTableWithName:column.className]) {
        return YES;
    }
    return [database executeUpdate:[NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN %@ %@", column.className, column.name, column.type]];
}

- (void)insertObject:(NSDictionary *)data ofClass:(NSString *)class withCallback:(InsertCompletionBlock)block;
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [queue inDatabase:^(FMDatabase *database) {
            if (![database tableExists:class]) {
                NSLog(@"Table does not exist %@", class);
                if (block) {
                    block (NO, nil, 0);
                }
            } else {
                BOOL result = [self performInsertIn:database for:class basedOn:data];
                if (!result) {
                    NSLog(@"%@", [database lastError]);
                }
                if (block) {
                    if (result) {
                        block (YES, nil, database.lastInsertRowId);
                    }
                    else {
                        block (NO, [database lastError], 0);
                    }
                }
            }
        }];
    }];
}

- (BOOL)performInsertIn:(FMDatabase *)database for:(NSString *)class basedOn:(NSDictionary *)data
{
	FMResultSet *resultSet = [database getTableSchema:class];
	NSMutableArray *columns = [NSMutableArray new];
	while ([resultSet next]) {
		[columns addObject:[resultSet stringForColumn:@"name"]];
	}
	
    [columns filterUsingPredicate:[NSPredicate predicateWithFormat:@"self in %@", data.allKeys]];
    
	NSMutableArray *placeholders = [NSMutableArray new];
    NSMutableArray *values = [NSMutableArray new];
	for (NSString *column in columns) {
        [placeholders addObject:@"?"];
        [values addObject:data[column]];
	}
	
	NSString *statement = [NSString stringWithFormat:@"insert into %@(%@) values(%@)", class,
						   [columns componentsJoinedByString:@","],
						   [placeholders componentsJoinedByString:@","]];
	
    NSLog(@"%@\nvalues: %@", statement, [values componentsJoinedByString:@","]);
    
	return [database executeUpdate:statement withArgumentsInArray:values];
}

- (void)updateObject:(NSDictionary *)data forQuery:(VMPersistantStorageQuery *)query withCallback:(VMCompletionBlock)block;
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [queue inDatabase:^(FMDatabase *database) {
            BOOL result = [self performUpdateIn:database forQuery:query basedOn:data];
            if (!result) {
                NSLog(@"%@", [database lastError]);
            }
            if (block) {
                block (result, result ? nil : [database lastError]);
            }
        }];
    }];
}

- (BOOL)performUpdateIn:(FMDatabase *)database forQuery:(VMPersistantStorageQuery *)query basedOn:(NSDictionary *)data
{
    FMResultSet *resultSet = [database getTableSchema:query.className];
    NSMutableArray *columns = [NSMutableArray new];
    while ([resultSet next]) {
        [columns addObject:[resultSet stringForColumn:@"name"]];
    }
    
    [columns filterUsingPredicate:[NSPredicate predicateWithFormat:@"self in %@", data.allKeys]];
    
    NSMutableArray *values = [NSMutableArray new];
    NSString *setStatement;
    for (NSString *column in columns) {
        if (!setStatement) {
            setStatement = @"set";
        }
        else {
            setStatement = [setStatement stringByAppendingString:@","];
        }
        setStatement = [NSString stringWithFormat:@"%@ %@=?", setStatement, column];
        [values addObject:data[column]];
    }
    [values addObjectsFromArray:query.whereArguments];
    NSString *statement = [NSString stringWithFormat:@"update %@ %@ %@", query.className, setStatement, query.whereStatement];
    NSLog(@"%@\nvalues: %@", statement, [values componentsJoinedByString:@","]);
    return [database executeUpdate:statement withArgumentsInArray:values];
}

- (void)updateOrInsertObject:(NSDictionary *)data forQuery:(VMPersistantStorageQuery *)query withCallback:(VMCompletionBlock)block;
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [queue inDatabase:^(FMDatabase *database) {
            BOOL result = [self performUpdateOrInsertIn:database forQuery:query basedOn:data];
            if (!result) {
                NSLog(@"%@", [database lastError]);
            }
            if (block) {
                block (result, result ? nil : [database lastError]);
            }
        }];
    }];
}

- (BOOL)performUpdateOrInsertIn:(FMDatabase *)database forQuery:(VMPersistantStorageQuery *)query basedOn:(NSDictionary *)data
{
    BOOL updateResult = [self performUpdateIn:database forQuery:query basedOn:data];
    if (updateResult) {
        if (database.changes == 0) {
            return [self performInsertIn:database for:query.className basedOn:data];
        }
        return YES;
    }
    return NO;
}

- (void)updateOrInsertObjects:(NSArray *)dictionaries forQueries:(NSArray *)queries withCallback:(VMCompletionBlock)block
{
    if (dictionaries.count != queries.count) {
        return;
    }
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [queue inTransaction:^(FMDatabase *database, BOOL *rollback) {
            for (NSInteger i = 0; i < dictionaries.count; i++) {
                BOOL result = [self performUpdateOrInsertIn:database forQuery:queries[i] basedOn:dictionaries[i]];
                if (!result) {
                    NSLog(@"%@", [database lastError]);
                    if (block) {
                        block (NO, [database lastError]);
                    }
                    *rollback = YES;
                    return;
                }
            }
            if (block) {
                block (YES, nil);
            }
        }];
    }];
}

- (void)fetchObjectsForQuery:(VMPersistantStorageQuery *)query withCallback:(VMArrayBlock)block;
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [queue inDatabase:^(FMDatabase *database) {
            NSLog(@"%@\nvalues: %@", query.fetchObjectsStatement, [query.whereArguments componentsJoinedByString:@","]);
            FMResultSet *resultSet = [database executeQuery:query.fetchObjectsStatement withArgumentsInArray:query.whereArguments];
            NSMutableArray *results = [NSMutableArray new];
            while ([resultSet next]) {
                [results addObject:[resultSet resultDictionary]];
            }
            if (block) {
                block (results);
            }
        }];
    }];
}

- (void)fetchCountOfObjectsForQuery:(VMPersistantStorageQuery *)query withCallback:(VMIntegerBlock)block;
{
    [self applyFunction:@"count" forValuesForKey:@"*" forQuery:query withCallback:^(id value) {
        if (block) {
            block (((NSNumber *)value).integerValue);
        }
    }];
}

- (void)fetchSumOfValuesForKey:(NSString *)key forQuery:(VMPersistantStorageQuery *)query withCallback:(VMNumberBlock)block;
{
    [self applyFunction:@"sum" forValuesForKey:key forQuery:query withCallback:block];
}

- (void)fetchMinOfValuesForKey:(NSString *)key forQuery:(VMPersistantStorageQuery *)query withCallback:(VMIdBlock)block;
{
    [self applyFunction:@"min" forValuesForKey:key forQuery:query withCallback:block];
}

- (void)fetchMaxOfValuesForKey:(NSString *)key forQuery:(VMPersistantStorageQuery *)query withCallback:(VMIdBlock)block;
{
    [self applyFunction:@"max" forValuesForKey:key forQuery:query withCallback:block];
}

- (void)fetchAvgValueForKey:(NSString *)key forQuery:(VMPersistantStorageQuery *)query withCallback:(VMNumberBlock)block
{
    [self applyFunction:@"avg" forValuesForKey:key forQuery:query withCallback:block];
}

- (void)applyFunction:(NSString *)functionName forValuesForKey:(NSString *)key forQuery:(VMPersistantStorageQuery *)query withCallback:(VMIdBlock)block
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [queue inDatabase:^(FMDatabase *database) {
            NSString *statement = [NSString stringWithFormat:@"select %@(%@) as %@ from %@ %@", functionName, key, functionName, query.className, query.whereStatement];
            NSLog(@"%@\nvalues: %@", statement, [query.whereArguments componentsJoinedByString:@","]);
            FMResultSet *resultSet = [database executeQuery:statement withArgumentsInArray:query.whereArguments];
            NSMutableArray *results = [NSMutableArray new];
            while ([resultSet next]) {
                [results addObject:[resultSet resultDictionary]];
            }
            if (results.count > 0) {
                if (block) {
                    if (![results.firstObject[functionName] isKindOfClass:[NSNull class]]) {
                        block (results.firstObject[functionName]);
                    }
                    else {
                        block (nil);
                    }
                }
            }
            else {
                NSLog(@"%@", [database lastError]);
            }
        }];
    }];
}

- (void)executeQueryWithString:(NSString *)queryString withArguments:(NSArray *)argumentsArray withCallback:(VMArrayBlock)block;
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [queue inDatabase:^(FMDatabase *database) {
            NSLog(@"%@", queryString);
            FMResultSet *resultSet = [database executeQuery:queryString withArgumentsInArray:argumentsArray];
            NSMutableArray *results = [NSMutableArray new];
            while ([resultSet next]) {
                [results addObject:[resultSet resultDictionary]];
            }
            if (results.count > 0) {
                if (block) {
                    block (results);
                }
            }
            else {
                NSLog(@"%@", [database lastError]);
            }
        }];
    }];
}

@end
