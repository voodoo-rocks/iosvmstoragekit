//
//  VMDatabaseColumn.m
//  iOSVM.PersistantStorage
//
//  Created by Olesya Kondrashkina on 25/06/15.
//  Copyright (c) 2015 Voodoo Mobile. All rights reserved.
//

#import "VMDatabaseColumn.h"

@implementation VMDatabaseColumn

- (id)initWithName:(NSString *)name andType:(NSString *)type andClassName:(NSString *)className;
{
    self = [self init];
    
    self.name = name;
    self.type = type;
    self.className = className;
    
    return self;
}

@end
