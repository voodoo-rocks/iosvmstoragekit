//
//  VMPersistantStorageQuery.m
//  iOSVM.Core
//
//  Created by Olesya Kondrashkina on 10/20/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMPersistantStorageQuery.h"

@implementation VMPersistantStorageQuery

- (id)initWithClass:(NSString *)class
{
    self = [super init];
    
    self.className = class;
    whereConditions = [NSMutableArray array];
    whereArguments = [NSMutableArray array];
    orderKeys = [NSMutableDictionary dictionary];
    
    return self;
}

- (void)whereKey:(NSString *)key equalTo:(id)object;
{
    [self addWhereKey:key andValue:object forComparisonSign:@"="];
}

- (void)whereKey:(NSString *)key notEqualTo:(id)object;
{
    [self addWhereKey:key andValue:object forComparisonSign:@"<>"];
}

- (void)whereKey:(NSString *)key lessThan:(id)object;
{
    [self addWhereKey:key andValue:object forComparisonSign:@"<"];
}

- (void)whereKey:(NSString *)key greaterThan:(id)object;
{
    [self addWhereKey:key andValue:object forComparisonSign:@">"];
}

- (void)whereKey:(NSString *)key lessThanOrEqualTo:(id)object;
{
    [self addWhereKey:key andValue:object forComparisonSign:@"<="];
}

- (void)whereKey:(NSString *)key greaterThanOrEqualTo:(id)object;
{
    [self addWhereKey:key andValue:object forComparisonSign:@">="];
}

- (void)whereKey:(NSString *)key containedIn:(NSArray *)objects;
{
    NSMutableArray *placeholders = [NSMutableArray array];
    for (id object in objects) {
        [placeholders addObject:@"?"];
        [whereArguments addObject:object];
    }
    NSString *statement = [NSString stringWithFormat:@"%@ in (%@)", key, [placeholders componentsJoinedByString:@","]];
    [whereConditions addObject:statement];
}

- (void)whereKeyNotNull:(NSString *)key;
{
    [whereConditions addObject:[NSString stringWithFormat:@"%@ not null", key]];
}

- (void)addWhereKey:(NSString *)key andValue:(id)value forComparisonSign:(NSString *)sign
{
    [whereConditions addObject:[NSString stringWithFormat:@"%@%@?", key, sign]];
    [whereArguments addObject:value];
}

- (void)orderBy:(NSString *)key ascending:(BOOL)ascending;
{
    orderKeys[key] = [NSNumber numberWithBool:ascending];
}

- (NSString *)whereStatement;
{
    if (whereConditions.count == 0) {
        return @"";
    }
    return [NSString stringWithFormat:@"where %@", [whereConditions componentsJoinedByString:@" and "]];
}

- (NSArray *)whereArguments;
{
    return whereArguments;
}

- (NSString *)fetchObjectsStatement
{
    NSString *statement = [NSString stringWithFormat:@"select * from %@", self.className];
    NSString *whereStatement = self.whereStatement;
    if (whereStatement) {
        statement = [NSString stringWithFormat:@"%@ %@", statement, whereStatement];
    }
    NSString *orderStatement;
    for (NSString *key in orderKeys) {
        if (!orderStatement) {
            orderStatement = @"order by";
        }
        else {
            orderStatement = [orderStatement stringByAppendingString:@" ,"];
        }
        orderStatement = [NSString stringWithFormat:@"%@ %@ %@", orderStatement, key, [[orderKeys valueForKey:key] boolValue] ? @"asc" : @"desc"];
    }
    if (orderStatement) {
        statement = [NSString stringWithFormat:@"%@ %@", statement, orderStatement];
    }
    if (self.limit) {
        statement = [NSString stringWithFormat:@"%@ limit %d", statement, (int)self.limit];
    }
    if (self.offset) {
        statement = [NSString stringWithFormat:@"%@ offset %d", statement, (int)self.offset];
    }
    return statement;
}

@end
