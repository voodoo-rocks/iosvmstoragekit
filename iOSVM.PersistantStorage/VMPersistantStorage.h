//
//  VMPersistantStorage.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 03/10/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <IOSVMCoreKit/VMCore.h>
#import "VMPersistantStorageQuery.h"
#import "VMDatabaseColumn.h"

@class FMDatabaseQueue;

typedef void(^InsertCompletionBlock)(BOOL succeeded, NSError *error, long long insertRowId);

@interface VMPersistantStorage : NSObject
{
	FMDatabaseQueue *queue;
}

+ (VMPersistantStorage *)sharedStorage;

- (void)addColumn:(VMDatabaseColumn *)column withCallback:(VMCompletionBlock)block;
- (void)addColumnsArray:(NSArray *)columns withCallback:(VMCompletionBlock)block;

- (void)insertObject:(NSDictionary *)data ofClass:(NSString *)class withCallback:(InsertCompletionBlock)block;

- (void)updateObject:(NSDictionary *)data forQuery:(VMPersistantStorageQuery *)query withCallback:(VMCompletionBlock)block;

- (void)updateOrInsertObject:(NSDictionary *)data forQuery:(VMPersistantStorageQuery *)query withCallback:(VMCompletionBlock)block;

- (void)updateOrInsertObjects:(NSArray *)dictionaries forQueries:(NSArray *)queries withCallback:(VMCompletionBlock)block;

- (void)fetchObjectsForQuery:(VMPersistantStorageQuery *)query withCallback:(VMArrayBlock)block;

- (void)fetchCountOfObjectsForQuery:(VMPersistantStorageQuery *)query withCallback:(VMIntegerBlock)block;

- (void)fetchSumOfValuesForKey:(NSString *)key forQuery:(VMPersistantStorageQuery *)query withCallback:(VMNumberBlock)block;
- (void)fetchAvgValueForKey:(NSString *)key forQuery:(VMPersistantStorageQuery *)query withCallback:(VMNumberBlock)block;

- (void)fetchMinOfValuesForKey:(NSString *)key forQuery:(VMPersistantStorageQuery *)query withCallback:(VMIdBlock)block;
- (void)fetchMaxOfValuesForKey:(NSString *)key forQuery:(VMPersistantStorageQuery *)query withCallback:(VMIdBlock)block;

- (void)executeQueryWithString:(NSString *)queryString withArguments:(NSArray *)argumentsArray withCallback:(VMArrayBlock)block;

@end
