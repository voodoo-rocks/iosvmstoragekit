//
//  VMPersistantStorageQuery.h
//  iOSVM.Core
//
//  Created by Olesya Kondrashkina on 10/20/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMPersistantStorageQuery : NSObject
{
    NSMutableArray *whereConditions;
    NSMutableArray *whereArguments;
    NSMutableDictionary *orderKeys;
}

- (id)initWithClass:(NSString *)class;

- (void)whereKey:(NSString *)key equalTo:(id)object;
- (void)whereKey:(NSString *)key notEqualTo:(id)object;
- (void)whereKey:(NSString *)key lessThan:(id)object;
- (void)whereKey:(NSString *)key greaterThan:(id)object;
- (void)whereKey:(NSString *)key lessThanOrEqualTo:(id)object;
- (void)whereKey:(NSString *)key greaterThanOrEqualTo:(id)object;
- (void)whereKey:(NSString *)key containedIn:(NSArray *)objects;
- (void)whereKeyNotNull:(NSString *)key;

- (void)orderBy:(NSString *)key ascending:(BOOL)ascending;

@property NSInteger limit;
@property NSInteger offset;

- (NSString *)fetchObjectsStatement;
- (NSString *)whereStatement;
- (NSArray *)whereArguments;

@property (copy) NSString *className;

@end
