Pod::Spec.new do |s|
  s.name             = "IOSVMStorageKit"
  s.version          = "1.0"
  s.summary          = "Useful set of timesaving tools"
  s.homepage         = "https://voodoo-mobile@bitbucket.org/voodoo-mobile/iosvmstoragekit.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Voodoo Mobile" => "public@voodoo-mobile.com" }
  s.source           = { :git => "https://voodoo-mobile@bitbucket.org/voodoo-mobile/iosvmstoragekit.git", branch:'master', tag:"1.0"}

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'iOSVM.PersistantStorage/**/*.{h,m}'
  s.resource_bundles = {
    'iOSVM.PersistantStorage' => ['Pod/Assets/*.png']
  }

    s.public_header_files = 'iOSVM.PersistantStorage/**/*.h'
    s.dependency 'IOSVMCoreKit', '~>1.0'
    s.dependency 'FMDB', '~> 2.5'
end
